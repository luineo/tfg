package telotraigodemipueblo

import grails.transaction.Transactional
import reactor.spring.context.annotation.*
import ttp.Mensaje
import ttp.TipoMensaje
import ttp.Usuario


/**
 * Created by jorge on 27/11/16.
 */

@Consumer
class EventToMessageService {

    @Transactional
    @Selector("nuevoAmigo")
    void onNuevoAmigo( event ){

        def usuario = Usuario.findByUsername("$event.usuario")
        if( !usuario )
            return

        def destinatario = Usuario.findByUsername("$event.destinatario")
        if( !destinatario )
            return

        Mensaje m = new Mensaje(texto:"Hola, ahora $usuario.nombre y tú ya estáis conectados",
                tipo: TipoMensaje.MENSAJE,
                remitente: usuario)
        m.addToDestinatarios(destinatario)
        m.save(flush:true)
    }

    @Transactional
    @Selector("nuevoLugar")
    void onNuevoLugar( event ){

        def usuario = Usuario.findByUsername("$event.usuario")
        if( !usuario || usuario.confia.size() == 0)
            return

        Mensaje m = new Mensaje(texto:"Hola, tu amigo $usuario.nombre ha añadido '$event.lugar' como lugar",
                tipo: TipoMensaje.LUGAR,
                remitente: usuario)
        usuario.confia.each{ Usuario amigo->
            m.addToDestinatarios(amigo)
        }
        m.save(flush:true)
    }


    @Transactional
    @Selector("nuevoViaje")
    void onNuevoViaje( event ){

        def usuario = Usuario.findByUsername("$event.usuario")
        if( !usuario || usuario.confia.size() == 0)
            return

        Mensaje m = new Mensaje(texto:"Hola, tu amigo $usuario.nombre va a viajar a '$event.lugar' proximamente",
                tipo: TipoMensaje.VIAJE,
                remitente: usuario)
        usuario.confia.each{ Usuario amigo->
            m.addToDestinatarios(amigo)
        }
        m.save(flush:true)
    }


    @Transactional
    @Selector("nuevoPedido")
    void onNuevoPedido( event ){
        def usuario = Usuario.findByUsername("$event.usuario")
        if( !usuario )
            return

        def destinatario = Usuario.findByUsername("$event.destinatario")
        if( !destinatario )
            return

        Mensaje m = new Mensaje(texto:"Hola, tu amigo $usuario.nombre quiere pedir $event.cantidad de $event.producto ",
                tipo: TipoMensaje.PEDIDO,
                remitente: usuario)
        m.addToDestinatarios(destinatario)
        m.save(flush:true)
    }


    @Transactional
    @Selector("pedidoAceptado")
    void onPedidoAceptado( event ){
        def usuario = Usuario.findByUsername("$event.usuario")
        if( !usuario )
            return

        def destinatario = Usuario.findByUsername("$event.destinatario")
        if( !destinatario )
            return

        Mensaje m = new Mensaje(texto:"Hola, tu amigo $usuario.nombre ha aceptado tu pedido de $event.cantidad de $event.producto ",
                tipo: TipoMensaje.MENSAJE,
                remitente: usuario)
        m.addToDestinatarios(destinatario)
        m.save(flush:true)
    }


    @Transactional
    @Selector("pedidoRechazado")
    void onPedidoRechazado( event ){
        def usuario = Usuario.findByUsername("$event.usuario")
        if( !usuario )
            return

        def destinatario = Usuario.findByUsername("$event.destinatario")
        if( !destinatario )
            return

        Mensaje m = new Mensaje(texto:"Hola, tu amigo $usuario.nombre NO PUEDE TRAERTE tu pedido de $event.cantidad de $event.producto ",
                tipo: TipoMensaje.MENSAJE,
                remitente: usuario)
        m.addToDestinatarios(destinatario)
        m.save(flush:true)
    }
}
