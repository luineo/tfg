package telotraigodemipueblo

/**
 * Created by jorge on 10/12/16.
 */
class TipoEvento {

    static String NUEVO_LUGAR = "nuevoLugar"
    static String NUEVO_VIAJE = "nuevoViaje"
    static String NUEVO_PEDIDO = "nuevoPedido"
    static String NUEVO_AMIGO = "nuevoAmigo"

    static String PEDIDO_ACEPTADO = "pedidoAceptado"
    static String PEDIDO_RECHAZADO = "pedidoRechazado"
}
