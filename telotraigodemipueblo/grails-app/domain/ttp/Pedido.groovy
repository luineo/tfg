package ttp

import grails.persistence.Entity

@Entity
class Pedido {

    Producto producto

    int cantidad

    Usuario solicitante

    static mapWith = "neo4j"

    static belongsTo = Viaje

    static constraints = {
    }

    static mapping = {
        dynamicAssociations true
    }
}
