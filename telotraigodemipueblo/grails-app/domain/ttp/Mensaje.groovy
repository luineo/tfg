package ttp
import grails.persistence.Entity

@Entity
class Mensaje {

    String tipo = TipoMensaje.MENSAJE
    String texto
    Date    enviado= new Date()
    Date    leido = null
    Usuario remitente

    static hasMany = [destinatarios:Usuario]

    void setRemitente(Usuario usuario){
        this['REMITENTE'] = [usuario]
    }

    Usuario getRemitente(){
        this['REMITENTE']
    }

    Mensaje addToDestinatarios(Usuario usuario){
        List destinatarios = this['DESTINATARIOS'] ?: []
        destinatarios.add usuario
        this['DESTINATARIOS'] = destinatarios
        this
    }

    Mensaje responde(Mensaje mensaje){
        List respuestas = this['RESPONDE'] ?: []
        respuestas.add mensaje
        this['RESPONDE']=respuestas
        this
    }

    static mapWith = "neo4j"

    static transients = ['remitente']

    static constraints = {
        tipo    blank:false
        texto   nullable: false, maxLength:500
        enviado nullable: false
        leido   nullable:true
    }

    static mapping = {
        dynamicAssociations true
        leido index:true
    }
}
