package ttp

/**
 * Created by jorge on 20/11/16.
 */
enum TipoMensaje {

    MENSAJE,
    INVITACION,
    LUGAR,
    VIAJE,
    PEDIDO,

}