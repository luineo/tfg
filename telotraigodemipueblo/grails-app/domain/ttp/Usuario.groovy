package ttp
import grails.persistence.Entity
import grails.rest.Resource

@Entity

class Usuario {

    String nombre       //<1>
    String username
    String password

    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    static hasMany = [          //<2>
            confia : Usuario,
            conoce : Lugar
    ]

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this)*.role
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
    }

    transient springSecurityService

    static transients = ['springSecurityService']

    static constraints = {          //<3>
        nombre nullable:false
        username nullable: false, email:true
        password nullable:false, password:true
    }

    static mapping = {              //<4>
        username index:true, unique:true
    }


    Usuario amigos(Usuario amigo){ //<5>
        this.addToConfia(amigo)
        amigo.addToConfia(this)
        this
    }

}
