//= wrapped
//= require /angular/angular
//= require /angular/angular-resource
//= require /angular/angular-route
//= require /angular/angular-ui-notification
//= require /telotraigodemipueblo/core/telotraigodemipueblo.core
//= require /telotraigodemipueblo/index/telotraigodemipueblo.index
//= require /telotraigodemipueblo/network/telotraigodemipueblo.network
angular.module("telotraigodemipueblo", [
    'ngResource',
    'ngRoute',
    'ui-notification',
    "telotraigodemipueblo.core",
    "telotraigodemipueblo.index",
    "telotraigodemipueblo.network"
]).config(
    function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/index/registro', {
              templateUrl: '/telotraigodemipueblo/index/register.html',
              controller: 'RegistroController'
            })
            .when('/index/loginform', {
              templateUrl: '/telotraigodemipueblo/index/loginform.html',
              controller: 'LoginController'
            })
            .when('/index/welcome', {
              templateUrl: '/telotraigodemipueblo/index/welcome.html',
              requireAuth: true
            })
            .when('/index/mislugares', {
              templateUrl: '/telotraigodemipueblo/index/lugarcrud.html',
              requireAuth: true
            })
            .when('/index/misproductos/:lugarId', {
                templateUrl: '/telotraigodemipueblo/index/productocrud.html',
                requireAuth: true
            })
            .when('/index/misviajes/:lugarId', {
                templateUrl: '/telotraigodemipueblo/index/viajecrud.html',
                requireAuth: true
            })
            .when('/index/networkviajes', {
                templateUrl: '/telotraigodemipueblo/network/viajes.html',
                requireAuth: true
            })
            .otherwise({
              templateUrl: '/telotraigodemipueblo/index/landingpage.html'
            });
        ;

        // configure html5 to get links working on jsfiddle
        $locationProvider.html5Mode(true);
    }
);