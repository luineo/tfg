//= wrapped

angular
    .module("telotraigodemipueblo.network")
    .controller("SearchController", SearchController);

function SearchController($log,Search,Notification,$location,$http) {
    var vm = this;

    vm.amigos = []

    vm.search = function(criterio, filtro){
        var args = filtro || {}
        args.id = criterio
        vm.amigos = Search.list(args)
    }

    vm.sendInvitation = function(user){
        var msg = {
            destinatarios:[{
                id:user.id
            }]
        };
        $http.post('/api/invitacion/send', msg).then(function (response) {
            Notification.success('Solicitud enviada')
         }, function(error){
            Notification.error("Error enviando solicitud")
         });
    }

    vm.search('usuario');
}
