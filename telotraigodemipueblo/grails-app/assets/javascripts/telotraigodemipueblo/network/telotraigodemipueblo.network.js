//= wrapped
//= require /angular/angular
//= require /angular/angular-resource
//= require /angular/angular-route
//= require /angular/angular-ui-notification
//= require /angular/angular-messages
//= require /telotraigodemipueblo/core/telotraigodemipueblo.core
//= require_self
//= require_tree domain
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree templates
//= require /angular/ui-bootstrap-tpls
angular.module("telotraigodemipueblo.network", [
    "telotraigodemipueblo.core",
    "ui.bootstrap.dropdown",
    "ui.bootstrap.collapse"]);

