//= wrapped

angular
    .module("telotraigodemipueblo.network")
    .factory("NetworkViaje", NetworkViaje);

function NetworkViaje($resource) {
    var NetworkViaje= $resource(
        "/api/viajes/:id",
        {"id": "@id"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return NetworkViaje;
}
