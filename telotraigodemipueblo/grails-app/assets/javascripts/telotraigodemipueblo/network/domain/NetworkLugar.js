//= wrapped

angular
    .module("telotraigodemipueblo.network")
    .factory("NetworkLugar", NetworkLugar);

function NetworkLugar($resource) {
    var NetworkLugar= $resource(
        "/api/lugares/:id",
        {"id": "@id"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return NetworkLugar;
}
