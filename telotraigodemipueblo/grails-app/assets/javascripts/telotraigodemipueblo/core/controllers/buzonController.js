//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .controller("BuzonController", BuzonController);

function BuzonController($log,Notification,$location,$http, Mensaje, $interval) {
    var vm = this;

    vm.mensajes = Mensaje.list({onlynews:true});

    vm.acceptInvitation = function(m){
        var msg = {
            mensaje:m.id
        };
        $http.post('/api/invitacion/accept', msg).then(function (response) {
            Notification.success('Ya sois amigos')
         }, function(error){
            Notification.error("Error enviando aceptación")
         });
    }

    vm.leido = function(m){
        m.leido = new Date().getTime()
        m.$update({},function(){
            vm.mensajes = Mensaje.list({onlynews:true});
        });
    }

    //refrescar inbox cada 10seg
    $interval(function(){
        vm.mensajes = Mensaje.list({onlynews:true});
    }, 1000*10);

}
