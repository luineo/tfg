//= wrapped
//= require /angular/angular
//= require /angular/angular-resource
//= require_self
//= require_tree domain
//= require_tree controllers
//= require_tree services
//= require_tree templates

angular.module("telotraigodemipueblo.core", ['ngResource','ui-notification'])
    .constant("contextPath", window.contextPath)
    .config(config);

function config($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
    $httpProvider.interceptors.push(httpRequestInterceptor);

    $httpProvider.interceptors.push('authInterceptorService');
}

function httpRequestInterceptor(contextPath,$log) {
    return {
        request: function (config) {
            if (!config.url.indexOf("/") == 0 && contextPath) {
                config.url = contextPath + "/" + config.url;
            }
            return config;
        }
    };
}
