//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .factory("Mensaje", Mensaje);

function Mensaje($resource) {
    var Mensaje = $resource(
        "/api/mensajes/:id",
        {"id": "@id"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return Mensaje;
}
