//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .factory("Pedido", Pedido);

function Pedido($resource) {
    var Pedido = $resource(
        "/api/conoce/:idLugar/viajes/:idViaje/pedidos/:id",
        {"id": "@id", "idLugar":"@idLugar", "idViaje":"@idViaje"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return Pedido;
}
