//= wrapped

angular
    .module("telotraigodemipueblo.index")
    .controller("LoginController", LoginController);

function LoginController($rootScope, $log,$http,Notification,$location,$window) {
     var vm = this;

    $rootScope.authenticated = false;
    vm.retry = 0;

     vm.login = function(user){
         $http.post('/api/login', {
             username: user.username,
             password: user.password
         }).then(function (response) {
             $rootScope.authenticated = true;
             $window.sessionStorage.token = response.data.access_token;
             $window.sessionStorage.username = response.data.username;
             $window.sessionStorage.roles = response.data.roles;
             $location.url('/index/welcome')
         }, function(error){
            $rootScope.authenticated = false;
            if( vm.retry++ > 3){
             $location.url('/index/landingpage')
             return
            }
            Notification.error("Usuario/pwd no válidos")
         });
     };

 }
