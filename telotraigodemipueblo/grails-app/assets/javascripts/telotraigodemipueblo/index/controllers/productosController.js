//= wrapped

angular
    .module("telotraigodemipueblo.index")
    .controller("ProductosController", ProductosController);

function ProductosController($log,Lugar, Producto,Notification,$location, $routeParams) {

    var vm = this;
    var REST = Producto;

    vm.lugarId = $routeParams.lugarId;
    vm.lugar = Lugar.get({id:vm.lugarId})

    vm.list = REST.list({ idLugar : vm.lugarId });

    vm.newInstance = new REST({ idLugar : vm.lugarId });

    vm.update = function(u){
        u.$update({ idLugar : vm.lugarId },function(){
            Notification.info("Campo actualizado correctamente")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });
    }

    vm.save = function() {
        $log.info(vm.newInstance)
        vm.newInstance.$save({ idLugar : vm.lugarId },function() {
              vm.list.push(angular.copy(vm.newInstance));
              vm.newInstance = new REST({idLugar : vm.lugarId });
              Notification.info("Campo actualizado correctamente")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });
    };

    vm.delete = function(instance) {
       instance.$delete({ idLugar : vm.lugarId },function() {
           var idx = vm.list.indexOf(instance);
           vm.list.splice(idx, 1);
           Notification.info("Elemento borrado")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });

    };


}
