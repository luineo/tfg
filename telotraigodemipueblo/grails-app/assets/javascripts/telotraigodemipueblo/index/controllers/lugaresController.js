//= wrapped

angular
    .module("telotraigodemipueblo.index")
    .controller("LugaresController", LugaresController);

function LugaresController($log,Lugar,Notification,$location) {

    var vm = this;
    vm.productos = function(u){
        $location.url('/index/misproductos/'+u.id);
    };

    vm.viajes = function(u){
        $location.url('/index/misviajes/'+u.id);
    };

    var REST = Lugar;

    vm.list = REST.list();
    vm.newInstance = new REST({});

    vm.update = function(u){
        u.$update({},function(){
            Notification.info("Campo actualizado correctamente")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });
    }

    vm.save = function() {
        $log.info(vm.newInstance)
        vm.newInstance.$save({},function() {
              vm.list.push(angular.copy(vm.newInstance));
              vm.newInstance = new REST({});
              Notification.info("Campo actualizado correctamente")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });
    };

    vm.delete = function(instance) {
       if(alert('Desea eliminar este pueblo de su lista')) return;
       instance.$delete({},function() {
           var idx = vm.list.indexOf(instance);
           vm.list.splice(idx, 1);
           Notification.info("Elemento borrado")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });

    };


}
