//= wrapped

angular
    .module("telotraigodemipueblo.index")
    .controller("ViajesController", ViajesController);

function ViajesController($log, Lugar, Viaje, Pedido, Producto, Notification, $location, $routeParams) {
    var vm = this;
    var REST = Viaje;

    vm.lugarId = $routeParams.lugarId;
    vm.lugar = Lugar.get({id:vm.lugarId})

    vm.list = REST.list({ idLugar : vm.lugarId });

    vm.newInstance = new REST({ idLugar : vm.lugarId });

    vm.update = function(u){
        u.$update({ idLugar : vm.lugarId },function(){
            Notification.info("Campo actualizado correctamente")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });
    }

    vm.save = function() {
        $log.info(vm.newInstance)
        vm.newInstance.$save({ idLugar : vm.lugarId },function() {
              vm.list.push(angular.copy(vm.newInstance));
              vm.newInstance = new REST({idLugar : vm.lugarId });
              Notification.info("Campo actualizado correctamente")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });
    };

    vm.delete = function(instance) {
       instance.$delete({ idLugar : vm.lugarId },function() {
           var idx = vm.list.indexOf(instance);
           vm.list.splice(idx, 1);
           Notification.info("Elemento borrado")
        },function(){
            Notification.error("Error, contacte con el administrador")
        });

    };

    vm.viajeSelected = function(viaje){
        vm.viaje = viaje;
        vm.listPedidos();
    }

    vm.listPedidos = function(){
        vm.pedidos = []
        Pedido.list({
            idLugar : vm.lugarId,
            idViaje : vm.viaje.id
        },function( data ){
            angular.forEach(data, function(value, key) {
                vm.pedidos.push(value)
                value.producto = Producto.get({idLugar:vm.lugarId, id:value.producto.id})
            })
        })
    }

    vm.acceptPedido = function(pedido){
        if( pedido.solicitante.id )
            pedido.solicitante = pedido.solicitante.id;
        pedido.$update({
            idLugar : vm.lugarId,
            idViaje : vm.viaje.id
        },function(){
            Notification.info("Notificación enviada al usuario")
            vm.listPedidos();
        })
    }

    vm.rejectPedido = function(pedido){
        pedido.$delete({
            idLugar : vm.lugarId,
            idViaje : vm.viaje.id
        },function(){
            Notification.info("Notificación enviada al usuario")
            vm.listPedidos();
        })
    }
}

