import ttp.Role

class BootStrap {

    def init = { servletContext ->

        Role.withTransaction{

            if( !Role.findByAuthority('ROLE_USER')){
                new Role(authority:'ROLE_USER').save(flush:true)
            }

            if( !Role.findByAuthority('ROLE_ADMIN')){
                new Role(authority:'ROLE_ADMIN').save(flush:true)
            }
        }

    }
    def destroy = {
    }
}
