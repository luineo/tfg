package telotraigodemipueblo

class UrlMappings {

    static mappings = {

        "/registration"(resource:'registration',excludes:['create','delete','edit'])

        "/api/search/$action?/$id?(.$format)?"(controller:'search')

        "/api/mensajes"(resources:'mensaje')

        "/api/me"(resource:'me')

        "/api/conoce"(resources:'conoce'){
            "/productos"(resources:'productoRest')
            "/viajes"(resources:'viajeRest'){
                "/pedidos"(resources:'pedidoRest')
            }
        }

        "/api/invitacion/$action?/$id?(.$format)?"(controller:'invitacion')

        "/api/lugares"(resources:'lugar'){
            "/productos"(resources:'producto')
        }

        "/api/viajes"(resources:'viaje'){
            "/pedidos"(resources:'pedido')
        }


        "/"(view: '/index')
        "/index/$action?/$resource?/$subresource?"(view:'/index')

        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
