package telotraigodemipueblo.api.core


import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import grails.transaction.Transactional
import ttp.Usuario

@Secured('ROLE_USER')
@Transactional(readOnly = true)
class MeController extends RestfulController<Usuario> {

    static responseFormats = ['json', 'xml']

    MeController() {
        super(Usuario, true)
    }

    SpringSecurityService springSecurityService

    protected Usuario queryForResource(Serializable id) {
        springSecurityService.currentUser as Usuario
    }
}
