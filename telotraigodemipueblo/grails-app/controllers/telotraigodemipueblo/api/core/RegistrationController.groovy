package telotraigodemipueblo.api.core

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.*
import ttp.Role
import ttp.UserRole
import ttp.Usuario

import static org.springframework.http.HttpStatus.*

/**
 * Created by jorge on 19/10/16.
 */
@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
@Transactional(readOnly = false)
class RegistrationController {

    static responseFormats = ['json']

    static allowedMethods = [save: "POST"]

    RegistrationController(){

    }

    def index(Integer max){
        respond [:]
    }

    @Transactional
    def save( ){
        def json = request.JSON
        if( Usuario.findByUsername(json.username) ){
            render status: CONFLICT
            return
        }
        Usuario instance = Usuario.newInstance()
        bindData instance, json

        if( instance.hasErrors() ){
            render status: NOT_ACCEPTABLE
        }else{

            Role role = Role.findByAuthority('ROLE_USER')
            instance.save ()
            UserRole.create(instance, role, true)

            render status: CREATED
        }

    }

}
