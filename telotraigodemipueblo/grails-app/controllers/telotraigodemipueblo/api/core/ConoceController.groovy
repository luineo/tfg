package telotraigodemipueblo.api.core

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import telotraigodemipueblo.TipoEvento
import ttp.Lugar
import ttp.Usuario

@Secured('ROLE_USER')
class ConoceController extends RestfulController<Lugar> {
    static responseFormats = ['json', 'xml']

    ConoceController() {
        super(Lugar)
    }

    SpringSecurityService springSecurityService

    protected getObjectToBind() {
        def instance = request.JSON
        instance.usuario = springSecurityService.currentUser as Usuario
        instance
    }

    protected Lugar saveResource(Lugar resource) {
        Usuario usuario = springSecurityService.currentUser as Usuario
        usuario.addToConoce resource
        usuario.save flush: true

        notify TipoEvento.NUEVO_LUGAR, [usuario: usuario.username, lugar: resource.nombre]

        resource
    }

    protected List<Lugar> listAllResources(Map params) {
        Usuario usuario = springSecurityService.currentUser as Usuario
        List<Lugar> list = usuario.conoce as List<Lugar>
        list
    }

    protected Lugar queryForResource(Serializable lugarId) {
        Usuario usuario = springSecurityService.currentUser as Usuario
        Lugar lugar = usuario.conoce.find{"$it.id" == "$lugarId"}
        lugar
    }
}
