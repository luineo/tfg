package telotraigodemipueblo.api.crud

/**
 * Created by jorge on 9/12/16.
 */
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import org.grails.web.json.JSONObject
import telotraigodemipueblo.TipoEvento
import ttp.Lugar
import ttp.Pedido
import ttp.Producto
import ttp.Usuario
import ttp.Viaje

@Secured('ROLE_USER')
class PedidoRestController extends RestfulController<Pedido> {
    static responseFormats = ['json', 'xml']

    PedidoRestController(){
        super(Pedido,false)
    }

    protected List<Viaje> listAllResources(Map params) {
        params.max = params.max ?: 10
        Viaje v = Viaje.get(params.viajeRestId)
        List<Pedido> ret  =[]
        ret.addAll v.pedidos
        ret ?: []
    }

    protected getObjectToBind() {
        def instance = request.JSON
        instance.viaje = Viaje.get(params.viajeRestId)
        instance.solicitante = Usuario.get(instance.solicitante)
        instance
    }

    SpringSecurityService springSecurityService

    protected Pedido queryForResource(Serializable id) {
        Pedido pedido = super.queryForResource(id)
        if( pedido && params.action == 'delete'){
            Usuario user = springSecurityService.currentUser as Usuario
            Viaje viaje= Viaje.get(params.viajeRestId)
            notify TipoEvento.PEDIDO_RECHAZADO, [
                    usuario: user.username,
                    viaje: viaje.id,
                    destinatario: pedido.solicitante.username,
                    producto: pedido.producto.texto,
                    cantidad: pedido.cantidad
            ]
        }
        pedido
    }

    protected Pedido saveResource(Pedido resource) {
        Viaje viaje= Viaje.get(params.viajeRestId)
        viaje.addToPedidos(resource)
        viaje.save flush: true

        Usuario user = springSecurityService.currentUser as Usuario
        notify TipoEvento.PEDIDO_ACEPTADO, [
                usuario: user.username,
                viaje: viaje.id,
                destinatario: resource.solicitante.username,
                producto: resource.producto.texto,
                cantidad: resource.cantidad
        ]

        resource
    }


}
