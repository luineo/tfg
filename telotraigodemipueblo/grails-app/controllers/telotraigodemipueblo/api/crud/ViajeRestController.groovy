package telotraigodemipueblo.api.crud


import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import telotraigodemipueblo.TipoEvento
import ttp.Lugar
import ttp.Usuario
import ttp.Viaje

@Secured('ROLE_USER')
class ViajeRestController extends RestfulController<Viaje> {
    static responseFormats = ['json', 'xml']

    ViajeRestController(){
        super(Viaje,false)
    }

    protected getObjectToBind() {
        def instance = request.JSON
        instance.lugar = Lugar.get(params.conoceId)
        instance.usuario = springSecurityService.currentUser as Usuario
        instance
    }

    protected List<Viaje> listAllResources(Map params) {
        params.max = params.max ?: 10
        Lugar l = Lugar.get(params.conoceId)
        List<Viaje> ret  =[]
        l?.recibe.each{ ret.add it }
        ret ?: []
    }


    SpringSecurityService springSecurityService


    protected Viaje saveResource(Viaje resource) {
        Usuario user = springSecurityService.currentUser as Usuario
        Lugar lugar = Lugar.get(params.conoceId)

        lugar.addToRecibe(resource)

        lugar.save flush: true
        user.save flush:true

        notify TipoEvento.NUEVO_VIAJE, [usuario: user.username, lugar: lugar.nombre, viaje: resource.texto]

        resource
    }

}

