package telotraigodemipueblo.api.crud

/**
 * Created by jorge on 8/12/16.
 */

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import org.grails.web.json.JSONObject
import ttp.Lugar
import ttp.Usuario

@Secured('ROLE_USER')
class LugarController extends RestfulController<Lugar>{

    LugarController(){
        super(Lugar,true)
    }

    SpringSecurityService springSecurityService

    protected List<Lugar> listAllResources(Map params) {
        params.max = params.max ?: 10
        Usuario user = springSecurityService.currentUser as Usuario

        List<Lugar> ret
        ret = Lugar.cypherStatic("""MATCH (u:Usuario {username:{1}})
                -[:CONFIA]->(o:Usuario)
                <-[:USUARIO]-(l:Lugar)          
                return l limit ${params.max}""",
                [user.username]).toList(Lugar)

        ret
    }

}
