package telotraigodemipueblo.api.network

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import ttp.Lugar
import ttp.Usuario
import ttp.Viaje

@Secured('ROLE_USER')
/**
 * Created by jorge on 7/12/16.
 */
class ViajeController extends RestfulController<Viaje> {
    static responseFormats = ['json', 'xml']

    ViajeController(){
        super(Viaje,true)
    }

    SpringSecurityService springSecurityService

    protected List<Viaje> listAllResources(Map params) {
        params.max = params.max ?: 10
        Date today = new Date()
        Usuario user = springSecurityService.currentUser as Usuario

        List<Viaje> ret
        ret = Viaje.cypherStatic("""MATCH (u:Usuario {username:{1}})
                -[:CONFIA]->(o:Usuario)
                <-[:USUARIO]-(l:Lugar)
                <-[:LUGAR]-(v:Viaje)
                where v.fechatope > {2} 
                return v limit ${params.max}""",
            [user.username, today]).toList(Viaje)

        ret
    }

}
