package telotraigodemipueblo.api.network

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import grails.transaction.Transactional
import ttp.Lugar
import ttp.Usuario

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE

@Secured('ROLE_USER')
@Transactional(readOnly = true)
class SearchController {

    static responseFormats = ['json']

    SpringSecurityService springSecurityService

    private List userList2Map(List<Usuario> usersList){
        Usuario user = springSecurityService.currentUser as Usuario
        List list = usersList*.properties
        list
    }

    def usuario() {
        int max = params.max ?: 5
        String like = params.like ?: ''
        Usuario user = springSecurityService.currentUser as Usuario
        List<Usuario> usersList = Usuario.cypherStatic("""
            match (m:Usuario { username: {1} } ) with m
            match (u:Usuario) with u, rand() as n
            where u.username <> {1} and u.username =~ {2}
            and not ((u)-[:CONFIA]->(m))
            return u order by n limit $max""", [user.username,'.*'+like+'.*'] as List)
                .toList(Usuario)

        [list:userList2Map(usersList)]
    }


    def lugar() {
        int max = params.max ?: 5
        String like = params.like ?: ''
        Usuario user = springSecurityService.currentUser as Usuario
        List<Usuario> usersList = Usuario.cypherStatic("""
            match (u:Usuario)-[:CONOCE]->(l:Lugar) with u, l, rand() as n
            where u.username <> {1} and  l.nombre =~ {2}
            return u order by n limit $max""",[ user.username, '.*'+like+'.*'] as List)
                .toList(Usuario)

        [list:userList2Map(usersList)]
    }
}
