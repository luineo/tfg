== /api/conoce/{lugar}/viajes

[[viajes]]Mediante este API la aplicación permite la típica gestión CRUD (alta, baja, modificación, borrado) de los
viajes de un lugar de un usuario.

=== Alta (POST)

Campos de solicitud:

include::{snippets}/crud/viaje/create/request-fields.adoc[]

Campos de respuesta:

include::{snippets}/crud/viaje/create/response-fields.adoc[]

Ejemplo:

include::{snippets}/crud/viaje/create/httpie-request.adoc[]

include::{snippets}/crud/viaje/create/http-response.adoc[]

=== Listar (GET)

Campos de solicitud:

_No requiere_

Campos de respuesta:

include::{snippets}/crud/viaje/list/response-fields.adoc[]

Ejemplo:

include::{snippets}/crud/viaje/list/httpie-request.adoc[]

include::{snippets}/crud/viaje/list/http-response.adoc[]

=== Ver (GET + id)

Campos de solicitud:

include::{snippets}/crud/viaje/get/path-parameters.adoc[]

Campos de respuesta:

include::{snippets}/crud/viaje/get/response-fields.adoc[]

Ejemplo:

include::{snippets}/crud/viaje/get/httpie-request.adoc[]

include::{snippets}/crud/viaje/get/http-response.adoc[]

=== Actualizar (PUT)

Campos de solicitud:

include::{snippets}/crud/viaje/update/path-parameters.adoc[]

Campos de respuesta:

include::{snippets}/crud/viaje/update/response-fields.adoc[]

Ejemplo:

include::{snippets}/crud/viaje/update/httpie-request.adoc[]

include::{snippets}/crud/viaje/update/http-response.adoc[]

=== Borrar (DELETE)

Campos de solicitud:

include::{snippets}/crud/viaje/delete/path-parameters.adoc[]

Campos de respuesta:

_No procede. Retorna 204 Not content_

Ejemplo:

include::{snippets}/crud/viaje/delete/httpie-request.adoc[]

include::{snippets}/crud/viaje/delete/http-response.adoc[]

