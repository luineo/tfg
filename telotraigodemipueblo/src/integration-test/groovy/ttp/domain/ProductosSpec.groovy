package ttp.domain

/**
 * Created by jorge on 26/11/16.
 */
import grails.test.mixin.integration.Integration
import grails.transaction.*
import spock.lang.*
import ttp.Lugar
import ttp.Usuario
import ttp.Producto

@Integration
@Rollback

class ProductosSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "test create lugar"() {

        given:
        long id = new Date().time
        Usuario user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')

        Lugar lugar = new Lugar(nombre:"$id",longitud:1,latitud:2)
        user.addToConoce(lugar)

        Producto producto = new Producto(texto:'un queso rico', precio:10)

        lugar.addToTiene(producto)

        user.save(flush:true)

        expect:
        Usuario.findByUsername("${id}@email.com")
        Lugar.findByNombre("$id")
        Lugar.findByNombre("$id").tiene.size()==1
    }
}
