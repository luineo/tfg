package ttp.docuapi
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.http.MediaType
import org.springframework.restdocs.request.ParameterDescriptor
import spock.lang.Shared
import ttp.Mensaje

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders

/**
 * Created by jorge on 8/12/16.
 */
@Integration
@Rollback
class LugarSpec  extends SpecificationSecure {

    @Override
    boolean isFriendRequired() {
        return true
    }

    @Override
    boolean isLugarRequired() {
        return true
    }

    @Override
    boolean isViajeRequired() {
        return true
    }

    @Override
    boolean isProductoRequired() {
        return true
    }

    @Shared String idLugar
    @Shared String idProducto

    void "get lugares mi network"(){
        given: "una red de amigos con lugares"

        when: "request la list de lugares disponibles"
        def then = givenRequest(documentBase("lugares/list",
                requestHeaders(FieldsDescriptors.requestSecureHeaders),
                requestParameters([parameterWithName('max').description('maximo lugares a recuperar')] as ParameterDescriptor[]),
                responseFields(fieldWithPath('[]').description('Un array de lugares').type(List.class).optional())
                //.andWithPrefix('[].recibe',fieldWithPath('[]').description('Un array de viajes').type(List.class).optional())
                //.andWithPrefix('[].recibe[].',FieldsDescriptors.responseViajeFields)
                        .andWithPrefix('[].tiene',fieldWithPath('[]').description('Un array de productos').type(List.class).optional())
                        .andWithPrefix('[].tiene[].',FieldsDescriptors.responseProductoFields)
                        .andWithPrefix('[].usuario.',FieldsDescriptors.responseViajeUsuarioFields)
                        .andWithPrefix('[].',FieldsDescriptors.responseLugarFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${friendAccessToken}")
                .when()
                .get("/api/lugares?max=10")
                .then()

        then: "hay lugares"
        then.assertThat()
            .statusCode(200)

        (idLugar=then.extract().path('[0].id') as String).length()

    }

    void "get 1 lugar mi network"(){
        given: "una red de amigos con lugares"

        when: "request un lugar"
        def then = givenRequest(documentBase("lugares/get",
                requestHeaders(FieldsDescriptors.requestSecureHeaders),
                responseFields(FieldsDescriptors.responseLugarFields)
                        //.andWithPrefix('recibe',fieldWithPath('[]').description('Un array de viajes').type(List.class).optional())
                        //.andWithPrefix('recibe[].',FieldsDescriptors.responseViajeFields)
                        .andWithPrefix('tiene',fieldWithPath('[]').description('Un array de productos').type(List.class).optional())
                        .andWithPrefix('tiene[].',FieldsDescriptors.responseProductoFields)
                        .andWithPrefix('usuario.',FieldsDescriptors.responseViajeUsuarioFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${friendAccessToken}")
                .when()
                .get("/api/lugares/${idLugar}")
                .then()
        then: "hay lugares"
        then.assertThat()
            .statusCode(200)

        then.extract().path('id') == idLugar

    }

    void "get productos de 1 lugar mi network"(){
        given: "una red de amigos con lugares"

        when: "request productos de un lugar"
        def then = givenRequest(documentBase("lugares/productos/list",
                requestHeaders(FieldsDescriptors.requestSecureHeaders),
                pathParameters(FieldsDescriptors.paramLugarId),
                responseFields(fieldWithPath("[]").description("Un array de productos"))
                        .andWithPrefix('[].', FieldsDescriptors.responseProductoFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${friendAccessToken}")
                .when()
                .get("/api/lugares/{idLugar}/productos", idLugar)
                .then()
        then: "hay productos"
        then.assertThat()
                .statusCode(200)
        (idProducto=then.extract().path('[0].id') as String).length()
    }

    void "get 1 producto de 1 lugar mi network"(){
        lastSpec=true
        given: "una red de amigos con lugares"

        when: "request un producto"
        def then = givenRequest(documentBase("lugares/productos/get",
                requestHeaders(FieldsDescriptors.requestSecureHeaders),
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramId,] as ParameterDescriptor[]),
                responseFields(FieldsDescriptors.responseProductoFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${friendAccessToken}")
                .when()
                .get("/api/lugares/{idLugar}/productos/{id}", idLugar, idProducto)
                .then()
        then: "hay productos"
        then.assertThat()
                .statusCode(200)
        idProducto==then.extract().path('id') as String
    }
}
