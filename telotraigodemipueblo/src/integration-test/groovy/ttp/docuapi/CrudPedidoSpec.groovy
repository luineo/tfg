package ttp.docuapi
import grails.test.mixin.integration.Integration
import groovy.json.JsonBuilder
import org.springframework.http.MediaType
import org.springframework.restdocs.request.ParameterDescriptor
import spock.lang.Shared
import ttp.Lugar
import ttp.Pedido

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters

/**
 * Created by jorge on 11/12/16.
 */
@Integration
class CrudPedidoSpec extends SpecificationSecure{

    @Override
    boolean isFriendRequired() {
        return true
    }

    @Override
    boolean isLugarRequired() {
        return true
    }

    @Override
    boolean isViajeRequired() {
        return true
    }

    @Override
    boolean isProductoRequired(){
        return true
    }

    @Shared String idPedido

    void cleanup(){
        if( lastSpec && idPedido){
            Pedido.get(idPedido)?.delete(flush:true)
        }
    }

    void "get pedidos de un viaje sin pedidos"(){
        given: "dado un usuario logeado y un viaje sin pedidos"

        def then = givenRequest(documentBase("crud/pedidos/empty",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramViajeId])
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/viajes/{idViaje}/pedidos",sharedLugar.id, sharedViaje.id)
                .then()

        expect: "no tiene pedidos"
        then.assertThat().statusCode(200)
        then.extract().path('[0]') == null
    }

    void "create pedido de #cantidad"(){
        given: "un viaje a realizar"

        def then = givenRequest(documentBase("crud/pedidos/create",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramViajeId])
//                requestFields(FieldsDescriptors.requestCreateViajeFields),
//                responseFields(FieldsDescriptors.responseViajeFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .body( new JsonBuilder([cantidad:cantidad, producto:sharedProducto.id, solicitante:friend.id]).toPrettyString() )
                .when()
                .post("/api/conoce/{idLugar}/viajes/{idViaje}/pedidos",sharedLugar.id, sharedViaje.id)
                .then()

        expect: "el pedido se crea"

        then.assertThat().statusCode(201)

        assert (idPedido=then.extract().path('id') as Long) != 0

        where:
        cantidad | dummy
         2      | null
    }

    void "get pedidos de un viaje con pedidos"(){
        given: "dado un usuario logeado y un viaje con pedidos"

        def then = givenRequest(documentBase("crud/pedidos/list",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramViajeId]),
                responseFields(fieldWithPath('[]').description('Un array de pedidos').type(List.class).optional())
                        .andWithPrefix('[].',FieldsDescriptors.responsePedidoFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/viajes/{idViaje}/pedidos",sharedLugar.id, sharedViaje.id)
                .then()

        expect: "tiene pedidos"
        then.assertThat().statusCode(200)
    }

    void "get pedido de un viaje "(){
        lastSpec=true
        given: "dado un usuario logeado y un viaje con pedidos"

        def then = givenRequest(documentBase("crud/pedidos/get",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramViajeId,FieldsDescriptors.paramId]),
                responseFields(FieldsDescriptors.responsePedidoFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/viajes/{idViaje}/pedidos/{id}",sharedLugar.id, sharedViaje.id, idPedido)
                .then()

        expect: "el pedido"
        then.assertThat().statusCode(200)
    }
/* debido a que grapenedb esta en free no nos da suficiente cancha para este test
    void "rechazar un pedido de un viaje "(){
        lastSpec=true
        given: "dado un usuario logeado y un viaje con pedidos"

        def then = givenRequest(documentBase("crud/pedidos/delete",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramViajeId,FieldsDescriptors.paramId])
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .delete("/api/conoce/{idLugar}/viajes/{idViaje}/pedidos/{id}",sharedLugar.id, sharedViaje.id, idPedido)
                .then()

        expect: "el pedido"
        then.assertThat().statusCode(204)
    }
*/
}
