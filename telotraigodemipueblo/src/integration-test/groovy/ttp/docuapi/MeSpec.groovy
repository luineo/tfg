package ttp.docuapi

import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.json.JsonBuilder
import org.springframework.http.MediaType
import ttp.Mensaje

import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters
/**
 * Created by jorge on 9/11/16.
 */
@Integration
class MeSpec extends SpecificationSecure {

    void 'how I am'(){
        lastSpec=true
        expect:
            givenRequest(documentBase("me",
                responseFields(FieldsDescriptors.responseIAmFields)
            ))
                    .contentType(MediaType.APPLICATION_JSON.toString())
                    .headers("Authorization":"Bearer ${accessToken}")
                    .when()
                    .get("/api/me")
                    .then()
                    .assertThat().statusCode(200)
    }
}
