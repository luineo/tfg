package ttp.docuapi

import grails.core.GrailsApplication
import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonBuilder
import org.junit.Rule
import org.junit.rules.TestName
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import spock.lang.Shared
import ttp.Role
import ttp.UserRole
import ttp.Usuario

import static com.jayway.restassured.RestAssured.given
import com.jayway.restassured.builder.RequestSpecBuilder
import com.jayway.restassured.specification.RequestSpecification
import org.springframework.beans.factory.annotation.Value
import org.springframework.restdocs.restassured.RestDocumentationFilter
import org.springframework.restdocs.snippet.Snippet
import spock.lang.Specification
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import static org.springframework.restdocs.operation.preprocess.Preprocessors.replacePattern
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.document
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.documentationConfiguration
import static org.springframework.restdocs.restassured.operation.preprocess.RestAssuredPreprocessors.modifyUris
import org.springframework.restdocs.JUnitRestDocumentation
/**
 * Created by jorge on 22/10/16.
 */
class SpecificationBase extends Specification{

    @Autowired
    GrailsApplication grailsApplication

    @Value('${local.server.port}')
    Integer serverPort

    protected final String HOST = 'telotraigodemipueblo.herokuapps.com'

    @Rule
    JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation('build/generated-snippets')

    protected RequestSpecification documentationSpec

    void setup() {
        this.documentationSpec = new RequestSpecBuilder()
                .addFilter(documentationConfiguration(restDocumentation)).build()
    }

    RestDocumentationFilter documentBase(String documentName, Snippet... snippets=null){
        if( snippets )
            document(documentName,
                    preprocessRequest(prettyPrint(),modifyUris().scheme("https").host(HOST).removePort(),
                            ConvertTokenBearerPreprocessor.tokenBearerPreprocessor()
                    ),
                    preprocessResponse(
                            ConvertTokenBearerPreprocessor.tokenBearerPreprocessor(),
                            ConvertTokenBearerContentModifier.convertTokenBearerContentModifier(),
                            prettyPrint()),
                    snippets
            )
        else
            document(documentName,
                    preprocessRequest(prettyPrint(),modifyUris().scheme("https").host(HOST).removePort(),
                            ConvertTokenBearerPreprocessor.tokenBearerPreprocessor()),
                    preprocessResponse(
                            ConvertTokenBearerPreprocessor.tokenBearerPreprocessor(),
                            ConvertTokenBearerContentModifier.convertTokenBearerContentModifier(),
                            prettyPrint())
            )
    }

    RequestSpecification givenRequest(RestDocumentationFilter filter){
        given(documentationSpec)
                .accept(MediaType.APPLICATION_JSON.toString())
                .filter(filter)
                .port(this.serverPort)
    }

}
