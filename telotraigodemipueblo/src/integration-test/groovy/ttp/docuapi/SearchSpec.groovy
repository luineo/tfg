package ttp.docuapi


import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.http.MediaType

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders

/**
 * Created by jorge on 1/11/16.
 */
@Integration
@Rollback
class SearchSpec extends SpecificationSecure {

    @Override
    boolean isFriendRequired() {
        return true
    }

    void "search #max by usuario like #like"(){
        given: "una busqueda por usuario"
        def given = givenRequest(documentBase("search_usuario",
                requestHeaders(FieldsDescriptors.requestSecureHeaders),
                requestParameters(FieldsDescriptors.requestSearchUsersFields),
                responseFields(fieldWithPath('[]').description('Un array de usuarios').type(List.class).optional())
                        .andWithPrefix('[].',FieldsDescriptors.responseUsersFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")

        when: "solicitamos parecidos a $like"
        def when = given.when()
                .get("/api/search/usuario?like=$like&max=$max")

        then: "hay resultados"
             when.then().assertThat().statusCode(200)

        where:
        like   | max
        'username' | 5
    }

    void "search #max by lugares like #like"(){
        lastSpec=true

        given: "una busqueda por usuario"
        def given = givenRequest(documentBase("search_lugar",
                requestHeaders(FieldsDescriptors.requestSecureHeaders),
                requestParameters(FieldsDescriptors.requestSearchUsersFields),
                responseFields(fieldWithPath('[]').description('Un array de usuarios').type(List.class).optional())
                        .andWithPrefix('[].',FieldsDescriptors.responseUsersFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")

        when: "solicitamos parecidos a $like"
        def when = given.when()
                .get("/api/search/lugar?like=$like&max=$max")

        then: "hay resultados"
        when.then().assertThat().statusCode(200)

        where:
        like   | max
        'almendralejo' | 5
    }

}
