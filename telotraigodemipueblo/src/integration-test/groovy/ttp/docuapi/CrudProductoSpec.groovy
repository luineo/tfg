package ttp.docuapi


import grails.test.mixin.integration.Integration
import groovy.json.JsonBuilder
import org.springframework.http.MediaType
import org.springframework.restdocs.request.ParameterDescriptor
import spock.lang.Shared
import ttp.Lugar

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters

/**
 * Created by jorge on 23/11/16.
 */
@Integration
class CrudProductoSpec extends SpecificationSecure {

    @Shared Long idProducto
    @Shared Long id

    void setup(){
        if( !id){
            Lugar.withTransaction {
                Lugar lugar = new Lugar(nombre: 'mi lugar', longitud: 1, latitud: 2)
                usuario.addToConoce(lugar)
                usuario.save(flush: true)
                id = lugar.id
            }
        }
    }

    void cleanup(){
        if( lastSpec ){
            Lugar.withTransaction{
                Lugar.findByName(lugarname)?.delete(flush:true)
            }
        }
    }

    void "get productos de un lugar vacio"(){
        given: "dado un usuario logeado y un lugar"

        def then = givenRequest(documentBase("crud/producto/empty",
                pathParameters(FieldsDescriptors.paramLugarId)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/productos",id)
                .then()

        expect: "no tiene productos"
        then.assertThat().statusCode(200)
        //then.extract().path('[0]') == null
    }

    void "create producto #producto con #precio"(){
        given: "un producto a crear"

        def then = givenRequest(documentBase("crud/producto/create",
                requestFields(FieldsDescriptors.requestCreateProductoFields),
                pathParameters(FieldsDescriptors.paramLugarId),
                responseFields(FieldsDescriptors.responseProductoFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .body( new JsonBuilder([texto:producto,precio:precio]).toPrettyString() )
                .when()
                .post("/api/conoce/{idLugar}/productos",id)
                .then()

        expect: "hay al menos un producto"

        then.assertThat().statusCode(201)

        assert (idProducto=then.extract().path('id') as Long) != 0

        where:
        producto    | precio
        "un queso rico" | 10
    }

    void "get productos de un lugar "(){
        given: "dado un usuario logeado y un lugar con productos"

        def then = givenRequest(documentBase("crud/producto/list",
                pathParameters(FieldsDescriptors.paramLugarId),
                responseFields(fieldWithPath("[]").description("Un array de productos"))
                        .andWithPrefix('[].', FieldsDescriptors.responseProductoFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/productos",id)
                .then()

        expect: "tiene productos"
        then.assertThat().statusCode(200)
        then.extract().path('[0].id')
    }

    void "get producto del usuario "(){
        given: "dado un usuario logeado"

        def then = givenRequest(documentBase("crud/producto/get",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramId,] as ParameterDescriptor[]),
                responseFields(FieldsDescriptors.responseProductoFields)
                 ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/productos/{id}", id, idProducto)
                .then()

        expect: "tiene un producto creado y es el mismo que creamos"
        then.assertThat().statusCode(200)
        idProducto == then.extract().path('id') as Long
    }

    void "update producto del usuario "(){

        given: "dado un usuario logeado"

        def then = givenRequest(documentBase("crud/producto/update",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramId,] as ParameterDescriptor[]),
                responseFields(FieldsDescriptors.responseProductoFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .body( new JsonBuilder([texto:'texto actualizado',precio:888]).toPrettyString() )
                .when()
                .put("/api/conoce/{idLugar}/productos/{id}",id, idProducto)
                .then()

        expect: "tiene un lugar creado y es el mismo que creamos"
        then.assertThat().statusCode(200)
        idProducto == then.extract().path('id') as Long
        888 == then.extract().path('precio') as int
    }

    void "delete producto del usuario "(){
        lastSpec=true

        given: "dado un usuario logeado con un producto"

        def then = givenRequest(documentBase("crud/producto/delete",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramId,] as ParameterDescriptor[]),
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .delete("/api/conoce/{idLugar}/productos/{id}",id, idProducto)
                .then()

        expect: "que se puede borrar el lugar"
        then.assertThat().statusCode(204)
    }
}
