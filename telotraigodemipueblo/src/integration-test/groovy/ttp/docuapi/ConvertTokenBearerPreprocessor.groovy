package ttp.docuapi

import org.springframework.http.HttpHeaders
import org.springframework.restdocs.operation.OperationRequest
import org.springframework.restdocs.operation.OperationRequestFactory
import org.springframework.restdocs.operation.OperationResponse
import org.springframework.restdocs.operation.OperationResponseFactory
import org.springframework.restdocs.operation.preprocess.OperationPreprocessor

/**
 * Created by jorge on 15/11/16.
 */
class ConvertTokenBearerPreprocessor implements OperationPreprocessor {

    private final OperationRequestFactory requestFactory = new OperationRequestFactory();

    private final OperationResponseFactory responseFactory = new OperationResponseFactory();

    @Override
    public OperationResponse preprocess(OperationResponse response) {
        return this.responseFactory.createFrom(response,
                removeHeaders(response.getHeaders()));
    }

    @Override
    public OperationRequest preprocess(OperationRequest request) {
        return this.requestFactory.createFrom(request,
                removeHeaders(request.getHeaders()));
    }

    private HttpHeaders removeHeaders(HttpHeaders originalHeaders) {
        HttpHeaders processedHeaders = new HttpHeaders();
        processedHeaders.putAll(originalHeaders);
        ['Authorization','access_token','refresh_token'].each { key ->
            if (processedHeaders.containsKey(key)) {
                String value = processedHeaders.get(key)[0]
                processedHeaders.set(key, value[0..Math.min(20,value.length())]+'....')
            }
        }
        return processedHeaders;
    }

    public static ConvertTokenBearerPreprocessor tokenBearerPreprocessor(){
        new ConvertTokenBearerPreprocessor()
    }
}
