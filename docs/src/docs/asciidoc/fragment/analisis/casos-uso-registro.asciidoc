

[plantuml,casos-de-uso-registro,png]
....
left to right direction
skinparam packageStyle rect
title Registro de usuario

actor Usuario as usuario
rectangle registro{
    usuario --- (registro)
    (registro) .> (usuario no existe) : include
    (usuario existe) <. (registro) : include
}

....
