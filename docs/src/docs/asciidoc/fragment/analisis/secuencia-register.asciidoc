A continuación se representan los tres diagramas de secuencia principales de la aplicación.

__Diagrama de secuencia donde un usuario anónimo desea registrarse en el sistema. Al no estar registrado el API
no puede securizarse mediante el intercambio de tokens__

[plantuml,secuencia-register,png]
....
title Register
autonumber

actor Anonimo
control Api
entity Usuario
database Neo4j

Anonimo -> Api  : register username
Api -> Usuario : findByUsername
Usuario -> Neo4j : match n

alt username not registered

Usuario --> Api: not found
Api -> Usuario: save
Usuario -> Neo4j : create
Api --> Anonimo: ok 201

else username registered

Usuario --> Api: found
Api --> Anonimo: error 409

end
....

<<<<

__Diagrama de secuencia donde un usuario anónimo desea identificarse en el sistema. Si la identificación es correcta
obtendrá un token para usar en futuras peticiones__

[plantuml,secuencia-login,png]
....
title Login
autonumber

actor Anonimo
control Api
control Security
entity Usuario
database Neo4j

Anonimo -> Api  : login username
Api -> Usuario : findByUsername
Usuario -> Neo4j : match n

alt username registered

Usuario --> Api: found
Api -> Api: check password

    alt successfull

        Api -> Security: generate token
        Security --> Api: token
        Api --> Anonimo: token

    else doesn't match

        Api --> Anonimo: 403

    end

else username not registered

Usuario --> Api: not found
Api --> Anonimo: error 403

end
....

<<<<

__Diagrama de secuencia donde un usuario identificado previamente y que disponde de un token,
 desea acceder a un recurso protegido (por ejemplo ver sus mensajes)__

[plantuml,secuencia-login,png]
....
title Login
autonumber

actor Usuario
control Api
control Security
entity Resource
database Neo4j

Usuario -> Api  : get resource

alt without token

    Api -->Usuario: error 401 unauthorized

else with token

    Api -> Security: validate token
    Security --> Api: token validated

    alt token expired

        Api -->Usuario: error 401 unauthorized

    else roles not enought

        Api --> Usuario: error 403, forbidden

    else roles granted

        Api -> Resource: get resource
        Resource -> Neo4j: match
        Api --> Usuario: json

    end

end
....

