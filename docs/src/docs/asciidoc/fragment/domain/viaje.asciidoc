
Entidad por la que un usuario avisa a su red de que va a viajar a un lugar en una fecha
 determinada

[source,groovy]
.Viaje.groovy
----
include::{sourcedir}/telotraigodemipueblo/grails-app/domain/ttp/Viaje.groovy[]
----
